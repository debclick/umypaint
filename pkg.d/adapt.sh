#package specific information for the desktop file
sed -i "/^Terminal=/i Icon=share/icons/hicolor/512x512/apps/mypaint.png" ${INSTALL_DIR}/*.desktop
#a short description for the package
sed -i "s/@description@/mypaint is for painting/" ${INSTALL_DIR}/manifest.json

#adapt to fit the needs of the package
